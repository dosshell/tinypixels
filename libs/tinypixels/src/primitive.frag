#version 300 es

precision mediump float; 

in vec4 our_color;

out vec4 FragColor;

void main()
{
    FragColor = our_color;
}
