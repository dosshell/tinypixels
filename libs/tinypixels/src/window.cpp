#include "window_internal.h"

#include "draw_internal.h"
#include "draw_internal.h"
#include "clock_internal.h"
#include "sound_internal.h"
#include "gui_internal.h"
#include "input_internal.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace tpx
{
struct WindowContext
{
    GLFWwindow* glfw_window;
    const char* title;
    bool initialized;
};
WindowContext self;

bool window_open(const char* title, int width, int height, bool full_screen, bool resizable, bool vsync, int max_fps)
{
    self = WindowContext();
    self.title = title;
    Time.update_period_target_us = max_fps != NO_MAX_FPS ? (2'000'000 + max_fps) / (2 * max_fps) : 0;

    if (!glfwInit())
    {
        return false;
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_RESIZABLE, resizable ? GLFW_TRUE : GLFW_FALSE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    self.glfw_window = glfwCreateWindow(width, height, title, full_screen ? glfwGetPrimaryMonitor() : nullptr, nullptr);

    if (self.glfw_window == nullptr)
    {
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(self.glfw_window);
    input_init();
    draw_init(width, height);
    glfwSwapInterval(vsync ? 1 : 0);
    bool audio_ok = sound_init();
    if (!audio_ok)
    {
        draw_destroy();
        return false;
    }

    nuklear_init();

    Time.start_timestamp_us = time_now();
    Time.frame_timestamp_us = Time.start_timestamp_us;
    self.initialized = true;

    draw_clear();
    input_update();

    return true;
}

bool window_close()
{
    nuklear_destroy();
    sound_destroy();
    draw_destroy();

    if (self.glfw_window != nullptr)
    {
        glfwDestroyWindow(self.glfw_window);
    }
    self.glfw_window = nullptr;

    glfwTerminate();

    return true;
}

bool acquire_context()
{
    glfwMakeContextCurrent(self.glfw_window);
    return true;
}

bool release_context()
{
    glfwMakeContextCurrent(nullptr);
    return true;
}

void window_swap_buffers()
{
    glfwSwapBuffers(self.glfw_window);
}

void* window_get_glfw_window()
{
    return (void*)self.glfw_window;
}

void window_update_resize()
{
    int width, height;
    glfwGetFramebufferSize(self.glfw_window, &width, &height);
    draw_set_size(width, height);
}

void window_flip()
{
    draw_flip();
    input_update();
}

} // namespace tpx