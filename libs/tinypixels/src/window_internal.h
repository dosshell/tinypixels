#pragma once
#include "window.h"

namespace tpx
{
void* window_get_glfw_window();
void window_swap_buffers();
void window_update_resize();
} // namespace tpx
