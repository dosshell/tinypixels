#include <cstdint>
#include <chrono>

#include <GLFW/glfw3.h>

namespace tpx
{
int64_t time_ticktock()
{
    static auto s_lastTimeStamp = std::chrono::high_resolution_clock::now();
    auto t2 = std::chrono::high_resolution_clock::now();
    int64_t elapsed = int64_t(std::chrono::duration_cast<std::chrono::microseconds>(t2 - s_lastTimeStamp).count());
    s_lastTimeStamp = t2;
    return elapsed;
}

int64_t time_now()
{
    return (int64_t)(glfwGetTime() * 1'000'000); // should be okey for atleast some thousands of years
}
} // namespace tpx
