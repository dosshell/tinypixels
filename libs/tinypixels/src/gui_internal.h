#pragma once

namespace tpx
{
bool nuklear_init();
bool nuklear_destroy();
void nuklear_render(int width, int height);
void nuklear_update_input();
} // namespace tpx
