#version 300 es

uniform ivec2 g_camera_position;
uniform vec2 g_camera_scale;
uniform int g_scale;
uniform bool g_use_pixel_per_units; // otherwise units_per_pixel

in vec2 v_quad_pos;
in ivec2 v_position;
in int v_subimage;

out vec2 frag_text_coord;
flat out int frag_subimage;

const vec2 UV[4] = vec2[4](
    vec2(0.0, 0.0),
    vec2(1.0, 0.0),
    vec2(0.0, 1.0),
    vec2(1.0, 1.0)
);

void main()
{
    ivec2 p = v_position;
    if (g_use_pixel_per_units)
    {
        p = p * g_scale;
    }
    else
    {
        // truncate against negative infinity
        if (-1 / 2 == 0)
        {
            p.x = v_position.x < 0 ? p.x - g_scale + 1: p.x;
            p.y = v_position.y < 0 ? p.y  - g_scale + 1: p.y;
        }
        p = p / g_scale;
    }
    vec2 delta_pos = vec2(p - g_camera_position) + vec2(v_quad_pos);
    vec2 screen_pos = delta_pos * g_camera_scale + vec2(-1.0, 1.0);
    gl_Position = vec4(screen_pos.x, screen_pos.y, 0.0, 1.0);

    frag_text_coord = UV[gl_VertexID];
    frag_subimage = v_subimage;
}
