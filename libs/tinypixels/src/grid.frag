#version 300 es

precision mediump float;

in vec2 frag_text_coord;
flat in int frag_subimage;
flat in int frag_mask_subimage;
uniform mediump sampler2DArray image_texture;
uniform mediump sampler2DArray mask_texture;
uniform bool use_mask;

out vec4 FragColor;

void main()
{
    vec4 tex_color = texture(image_texture, vec3(frag_text_coord, frag_subimage));
    if (use_mask)
    {
        vec4 mask_color = texture(mask_texture, vec3(frag_text_coord, frag_mask_subimage));
        FragColor = tex_color * mask_color;
    }
    else
    {
        FragColor = tex_color;
    }
}
