#include "gui_internal.h"

#include "draw_internal.h"
#include "input_internal.h"
#include "shaders.h"

#include <gllpp.hpp>
#include <nuklear.h>

#include <cassert>
#include <cstddef>
#include <cstring>

namespace tpx
{

struct nk_context Nuklear;

struct nk_glfw_device
{
    struct nk_buffer cmds;
    struct nk_draw_null_texture tex_null;
    GLuint vbo, vao, ebo;
    GLuint prog;
    GLuint vert_shdr;
    GLuint frag_shdr;
    GLint attrib_pos;
    GLint attrib_uv;
    GLint attrib_col;
    GLint uniform_tex;
    GLint uniform_proj;
    GLuint font_tex;
    struct nk_font_atlas atlas;
};

static nk_glfw_device self;

struct nk_glfw_vertex
{
    float position[2];
    float uv[2];
    nk_byte col[4];
};

constexpr int max_vertex_buffer = 512 * 1024;
constexpr int max_element_buffer = 128 * 1024;

static char vertices[max_vertex_buffer];
static char elements[max_element_buffer];

bool nuklear_init()
{
    if (nk_init_default(&Nuklear, 0) == 0)
    {
        return false;
    }

    GLint status;
    struct nk_glfw_device* dev = &self;
    nk_buffer_init_default(&dev->cmds);
    dev->prog = glCreateProgram();
    dev->vert_shdr = glCreateShader(GL_VERTEX_SHADER);
    dev->frag_shdr = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(dev->vert_shdr, 1, &shader_vertex_gui_src, 0);
    glShaderSource(dev->frag_shdr, 1, &shader_fragment_gui_src, 0);
    glCompileShader(dev->vert_shdr);
    glCompileShader(dev->frag_shdr);
    glGetShaderiv(dev->vert_shdr, GL_COMPILE_STATUS, &status);
    assert(status == GL_TRUE);
    glGetShaderiv(dev->frag_shdr, GL_COMPILE_STATUS, &status);
    assert(status == GL_TRUE);
    glAttachShader(dev->prog, dev->vert_shdr);
    glAttachShader(dev->prog, dev->frag_shdr);
    glLinkProgram(dev->prog);
    glGetProgramiv(dev->prog, GL_LINK_STATUS, &status);
    assert(status == GL_TRUE);
    assert(glGetError() == GL_NO_ERROR);

    dev->uniform_tex = glGetUniformLocation(dev->prog, "Texture");
    dev->uniform_proj = glGetUniformLocation(dev->prog, "ProjMtx");
    dev->attrib_pos = glGetAttribLocation(dev->prog, "Position");
    dev->attrib_uv = glGetAttribLocation(dev->prog, "TexCoord");
    dev->attrib_col = glGetAttribLocation(dev->prog, "Color");
    assert(glGetError() == GL_NO_ERROR);

    {
        /* buffer setup */
        GLsizei vs = sizeof(struct nk_glfw_vertex);
        size_t vp = offsetof(struct nk_glfw_vertex, position);
        size_t vt = offsetof(struct nk_glfw_vertex, uv);
        size_t vc = offsetof(struct nk_glfw_vertex, col);

        glGenBuffers(1, &dev->vbo);
        glGenBuffers(1, &dev->ebo);
        glGenVertexArrays(1, &dev->vao);
        assert(glGetError() == GL_NO_ERROR);

        glBindVertexArray(dev->vao);
        glBindBuffer(GL_ARRAY_BUFFER, dev->vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dev->ebo);

        glBufferData(GL_ARRAY_BUFFER, max_vertex_buffer, NULL, GL_STREAM_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, max_element_buffer, NULL, GL_STREAM_DRAW);

        assert(glGetError() == GL_NO_ERROR);

        glEnableVertexAttribArray((GLuint)dev->attrib_pos);
        assert(glGetError() == GL_NO_ERROR);

        glEnableVertexAttribArray((GLuint)dev->attrib_uv);
        assert(glGetError() == GL_NO_ERROR);

        glEnableVertexAttribArray((GLuint)dev->attrib_col);
        assert(glGetError() == GL_NO_ERROR);

        glVertexAttribPointer((GLuint)dev->attrib_pos, 2, GL_FLOAT, GL_FALSE, vs, (void*)vp);
        glVertexAttribPointer((GLuint)dev->attrib_uv, 2, GL_FLOAT, GL_FALSE, vs, (void*)vt);
        glVertexAttribPointer((GLuint)dev->attrib_col, 4, GL_UNSIGNED_BYTE, GL_TRUE, vs, (void*)vc);
    }
    assert(glGetError() == GL_NO_ERROR);

    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    assert(glGetError() == GL_NO_ERROR);

    nk_font_atlas_init_default(&self.atlas);
    nk_font_atlas_begin(&self.atlas);

    const void* image;
    int w;
    int h;
    image = nk_font_atlas_bake(&self.atlas, &w, &h, NK_FONT_ATLAS_RGBA32);

    glGenTextures(1, &dev->font_tex);
    glBindTexture(GL_TEXTURE_2D_ARRAY, dev->font_tex);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, w, h, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, w, h, 1, GL_RGBA, GL_UNSIGNED_BYTE, image);
    nk_font_atlas_end(&self.atlas, nk_handle_id((int)self.font_tex), &self.tex_null);
    if (self.atlas.default_font)
        nk_style_set_font(&Nuklear, &self.atlas.default_font->handle);

    return true;
}

bool nuklear_destroy()
{
    return false;
}

void nuklear_render(int width, int height)
{
    struct nk_glfw_device* dev = &self;
    struct nk_buffer vbuf;
    struct nk_buffer ebuf;
    GLfloat ortho[4][4] = {
        {2.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, -2.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {-1.0f, 1.0f, 0.0f, 1.0f},
    };
    ortho[0][0] /= (GLfloat)width;
    ortho[1][1] /= (GLfloat)height;

    /* setup global state */
    GLboolean was_cull_face;
    GLboolean was_depth_test;
    GLboolean was_scissor_test;

    glGetBooleanv(GL_CULL_FACE, &was_cull_face);
    glGetBooleanv(GL_DEPTH_TEST, &was_depth_test);
    glGetBooleanv(GL_SCISSOR_TEST, &was_scissor_test);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);

    /* setup program */
    glUseProgram(dev->prog);
    glUniform1i(dev->uniform_tex, 0);
    glUniformMatrix4fv(dev->uniform_proj, 1, GL_FALSE, &ortho[0][0]);
    glViewport(0, 0, width, height); // not needed?
    {
        /* convert from command queue into draw list and draw to screen */
        const struct nk_draw_command* cmd;
        nk_size offset = 0;

        /* allocate vertex and element buffer */
        glBindVertexArray(dev->vao);

        glBindBuffer(GL_ARRAY_BUFFER, dev->vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dev->ebo);

        /* load draw vertices & elements directly into vertex + element buffer */
        {
            /* fill convert configuration */
            struct nk_convert_config config;
            static const struct nk_draw_vertex_layout_element vertex_layout[] = {
                {NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(struct nk_glfw_vertex, position)},
                {NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(struct nk_glfw_vertex, uv)},
                {NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_OFFSETOF(struct nk_glfw_vertex, col)},
                {NK_VERTEX_LAYOUT_END}};
            memset(&config, 0, sizeof(config));
            config.vertex_layout = vertex_layout;
            config.vertex_size = sizeof(struct nk_glfw_vertex);
            config.vertex_alignment = NK_ALIGNOF(struct nk_glfw_vertex);
            config.tex_null = dev->tex_null;
            config.circle_segment_count = 22;
            config.curve_segment_count = 22;
            config.arc_segment_count = 22;
            config.global_alpha = 1.0f;
            config.shape_AA = NK_ANTI_ALIASING_ON;
            config.line_AA = NK_ANTI_ALIASING_ON;

            /* setup buffers to load vertices and elements */
            nk_buffer_init_fixed(&vbuf, vertices, (size_t)max_vertex_buffer);
            nk_buffer_init_fixed(&ebuf, elements, (size_t)max_element_buffer);
            nk_convert(&Nuklear, &self.cmds, &vbuf, &ebuf, &config);
        }

        glBufferSubData(GL_ARRAY_BUFFER, 0, (size_t)max_vertex_buffer, vertices);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, (size_t)max_element_buffer, elements);

        /* iterate over and execute each draw command */
        nk_draw_foreach(cmd, &Nuklear, &dev->cmds)
        {
            if (!cmd->elem_count)
                continue;
            glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)cmd->texture.id);
            assert(glGetError() == GL_NO_ERROR);

            glScissor((GLint)(cmd->clip_rect.x),
                      (GLint)((height - (GLint)(cmd->clip_rect.y + cmd->clip_rect.h))),
                      (GLint)(cmd->clip_rect.w),
                      (GLint)(cmd->clip_rect.h));
            assert(glGetError() == GL_NO_ERROR);

            glDrawElements(GL_TRIANGLES, (GLsizei)cmd->elem_count, GL_UNSIGNED_SHORT, (const void*)offset);
            assert(glGetError() == GL_NO_ERROR);

            offset += cmd->elem_count * sizeof(nk_draw_index);
            assert(glGetError() == GL_NO_ERROR);
        }
        nk_clear(&Nuklear);
        nk_buffer_clear(&dev->cmds);
    }

    /* default OpenGL state */
    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    if (was_cull_face)
    {
        glEnable(GL_CULL_FACE);
    }
    if (was_depth_test)
    {
        glEnable(GL_DEPTH_TEST);
    }
    if (!was_scissor_test)
    {
        glDisable(GL_SCISSOR_TEST);
    }
    assert(glGetError() == GL_NO_ERROR);
}

void nuklear_update_input()
{
    nk_input_begin(&Nuklear);

    nk_input_motion(&Nuklear, Input.mouse_x, Input.mouse_y);
    nk_input_button(&Nuklear, NK_BUTTON_LEFT, Input.mouse_x, Input.mouse_y, Input.is_down[Button::MOUSE_1]);
    nk_input_button(&Nuklear, NK_BUTTON_RIGHT, Input.mouse_x, Input.mouse_y, Input.is_down[Button::MOUSE_2]);
    nk_input_button(&Nuklear, NK_BUTTON_MIDDLE, Input.mouse_x, Input.mouse_y, Input.is_down[Button::MOUSE_3]);
    nk_input_scroll(&Nuklear, {0.0, (float)Input.scroll});
    nk_input_end(&Nuklear);
}
} // namespace tpx
