#pragma once

#include "draw.h"

struct GLFWwindow;

namespace tpx
{
void draw_init(int width, int height);
void draw_destroy();
void draw_set_size(int width, int height);
} // namespace tpx
