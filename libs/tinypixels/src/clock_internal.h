#pragma once

#include "clock.h"

namespace tpx
{
// ticktock in microseconds
int64_t time_ticktock();
} // namespace tpx
