#version 300 es

uniform ivec2 g_camera_position;
uniform vec2 g_camera_scale;
uniform int g_scale;
uniform bool g_use_pixel_per_units; // otherwise units_per_pixel
uniform ivec2 g_position;
uniform ivec2 g_img_size;
uniform int g_columns;

in int v_subimage;
in int v_mask_subimage;

out vec2 frag_text_coord;
flat out int frag_subimage;
flat out int frag_mask_subimage;

const vec2 UV[4] = vec2[4](
    vec2(0.0, 0.0),
    vec2(1.0, 0.0),
    vec2(0.0, 1.0),
    vec2(1.0, 1.0)
);

void main()
{
    vec2 QUAD[4] = vec2[4](
        vec2(0.0, 0.0),
        vec2(g_img_size[0], 0.0),
        vec2(0.0, g_img_size[1]),
        vec2(g_img_size[0], g_img_size[1])
    );

    ivec2 p = g_position;
    if (g_use_pixel_per_units)
    {
        p = p * g_scale;
    }
    else
    {
        // truncate against negative infinity
        if (-1 / 2 == 0)
        {
            p.x = g_position.x < 0 ? p.x - g_scale + 1: p.x;
            p.y = g_position.y < 0 ? p.y  - g_scale + 1: p.y;
        }
        p = p / g_scale;
    }
    int column = gl_InstanceID % g_columns;
    int row = gl_InstanceID / g_columns;

    vec2 top_left = vec2(p - g_camera_position);
    vec2 space_pos = top_left + QUAD[gl_VertexID] + vec2(column * g_img_size[0], row * g_img_size[1]);

    vec2 screen_pos = space_pos * g_camera_scale + vec2(-1.0, 1.0);
    gl_Position = vec4(screen_pos.x, screen_pos.y, 0.0, 1.0);

    frag_text_coord = UV[gl_VertexID];
    frag_subimage = v_subimage;
    frag_mask_subimage = v_mask_subimage;
}
