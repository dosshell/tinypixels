#version 300 es

uniform ivec2 g_camera_position;
uniform vec2 g_camera_scale;
uniform int g_scale;
uniform bool g_use_pixel_per_units; // otherwise units_per_pixel
uniform vec4 g_color;

in ivec2 v_position;

out vec4 our_color;

void main()
{
    ivec2 p = v_position;
    if (g_use_pixel_per_units)
    {
        p = p * g_scale;
    }
    else
    {
        // truncate against negative infinity
        if (-1 / 2 == 0)
        {
            p.x = v_position.x < 0 ? p.x - g_scale + 1: p.x;
            p.y = v_position.y < 0 ? p.y  - g_scale + 1: p.y;
        }
        p = p / g_scale;
    }

    vec2 delta_pos = vec2(p - g_camera_position) + vec2(0.5, 0.5);
    vec2 screen_pos = delta_pos * g_camera_scale + vec2(-1.0, 1.0);
    gl_Position = vec4(screen_pos.x, screen_pos.y, 0.0, 1.0);
    our_color = g_color;
}
