#include "draw_internal.h"

#include "gui_internal.h"
#include "window_internal.h"
#include "clock_internal.h"

#include "shaders.h"

#include <gllpp.hpp>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <nuklear.h>

#include <iostream>
#include <cassert>
#include <thread>
#include <chrono>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <sys/mman.h>
#include <fcntl.h>  // open
#include <unistd.h> //close
#endif

namespace
{
struct ShaderProgram
{
    GLuint program;
    GLuint vertex_shader;
    GLuint fragment_shader;
};

struct PrimitiveShader
{
    ShaderProgram shader;
    GLuint vao;
    GLuint vbo_positions;
    GLint attrib_camera_position;
    GLint attrib_camera_scale;
    GLint attrib_scale;
    GLint attrib_use_pixel_per_units; // multiply or divide
    GLint attrib_position;
    GLint attrib_color;
};

struct SpriteShader
{
    ShaderProgram shader;
    GLuint vao;
    GLuint vbo_quad; // fixed data, not using instancing index
    GLuint vbo_position;
    GLuint vbo_subimage;

    GLint attrib_quad_pos;
    GLint attrib_position;
    GLint attrib_subimage;
    GLint attrib_camera_position;
    GLint attrib_camera_scale;
    GLint attrib_scale;
    GLint attrib_use_pixel_per_units;
};

struct GridShader
{
    ShaderProgram shader;
    GLuint vao;
    GLuint vbo_subimage;
    GLuint vbo_mask_subimage;

    GLint attrib_camera_position;
    GLint attrib_camera_scale;
    GLint attrib_scale;
    GLint attrib_use_pixel_per_units;
    GLint attrib_position;
    GLint attrib_img_size;
    GLint attrib_subimage;
    GLint attrib_mask_subimage;
    GLint attrib_columns;
    GLint attrib_image_texture;
    GLint attrib_mask_texture;
    GLint attrib_use_mask;
};

struct DrawContext
{
    PrimitiveShader shader_primitive;
    SpriteShader shader_sprite;
    GridShader shader_grid;

    int width;
    int height;
    float width_scale;
    float height_scale;
};

static DrawContext draw_ctx{};

void draw_create_shader(ShaderProgram* program, const char* vertex_shader, const char* fragment_shader)
{
    program->vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(program->vertex_shader, 1, &vertex_shader, nullptr);
    glCompileShader(program->vertex_shader);

#ifndef NDEBUG
    {
        int success;
        glGetShaderiv(program->vertex_shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            char info_log[512];
            glGetShaderInfoLog(program->vertex_shader, 512, nullptr, info_log);
            std::cout << info_log << std::endl;
            assert(success);
        }
    }
#endif // DEBUG

    program->fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(program->fragment_shader, 1, &fragment_shader, nullptr);
    glCompileShader(program->fragment_shader);

#ifndef NDEBUG
    {
        int success;
        glGetShaderiv(program->fragment_shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            char info_log[512];
            glGetShaderInfoLog(program->fragment_shader, 512, nullptr, info_log);
            std::cout << info_log << std::endl;
            assert(success);
        }
    }
#endif // DEBUG

    program->program = glCreateProgram();
    glAttachShader(program->program, program->vertex_shader);
    glAttachShader(program->program, program->fragment_shader);
    glLinkProgram(program->program);

#ifndef NDEBUG
    {
        int success;
        glGetProgramiv(program->program, GL_LINK_STATUS, &success);
        if (!success)
        {
            char info_log[512];
            glGetShaderInfoLog(program->fragment_shader, 512, nullptr, info_log);
            std::cout << info_log << std::endl;
            assert(success);
        }
    }
#endif // DEBUG

    assert(glGetError() == GL_NO_ERROR);
}

void draw_destroy_shader(ShaderProgram* program)
{
    assert(program != nullptr);
    glDeleteProgram(program->program);
    glDeleteShader(program->vertex_shader);
    glDeleteShader(program->fragment_shader);
    assert(glGetError() == GL_NO_ERROR);
}
} // namespace

namespace tpx
{
CameraType Camera;

void draw_init(int width, int height)
{
    glInit();

    // Setup OpenGL
    glViewport(0, 0, width, height);
    assert(glGetError() == GL_NO_ERROR);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    assert(glGetError() == GL_NO_ERROR);

    // Setup camera
    Camera = CameraType{0, 0, width, height};
    draw_ctx.width = width;
    draw_ctx.height = height;
    draw_ctx.width_scale = 2.0f / Camera.width;
    draw_ctx.height_scale = -2.0f / Camera.height;

    // Setup primitive shader
    draw_create_shader(&draw_ctx.shader_primitive.shader, shader_vertex_primitive_src, shader_fragment_primitive_src);

    glGenVertexArrays(1, &draw_ctx.shader_primitive.vao);
    glBindVertexArray(draw_ctx.shader_primitive.vao);
    assert(glGetError() == GL_NO_ERROR);

    draw_ctx.shader_primitive.attrib_camera_position = glGetUniformLocation(draw_ctx.shader_primitive.shader.program, "g_camera_position");
    draw_ctx.shader_primitive.attrib_camera_scale = glGetUniformLocation(draw_ctx.shader_primitive.shader.program, "g_camera_scale");
    draw_ctx.shader_primitive.attrib_scale = glGetUniformLocation(draw_ctx.shader_primitive.shader.program, "g_scale");
    draw_ctx.shader_primitive.attrib_use_pixel_per_units = glGetUniformLocation(draw_ctx.shader_primitive.shader.program, "g_use_pixel_per_units");
    draw_ctx.shader_primitive.attrib_color = glGetUniformLocation(draw_ctx.shader_primitive.shader.program, "g_color");

    draw_ctx.shader_primitive.attrib_position = glGetAttribLocation(draw_ctx.shader_primitive.shader.program, "v_position");
    if (draw_ctx.shader_primitive.attrib_position != -1)
    {
        glGenBuffers(1, &draw_ctx.shader_primitive.vbo_positions);
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_primitive.vbo_positions);
        glVertexAttribIPointer(draw_ctx.shader_primitive.attrib_position, 2, GL_INT, 0, (void*)0);
        glEnableVertexAttribArray(draw_ctx.shader_primitive.attrib_position);
        assert(glGetError() == GL_NO_ERROR);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    assert(glGetError() == GL_NO_ERROR);

    // Setup sprite shader
    draw_create_shader(&draw_ctx.shader_sprite.shader, shader_vertex_sprite_src, shader_fragment_sprite_src);
    glGenVertexArrays(1, &draw_ctx.shader_sprite.vao);
    glBindVertexArray(draw_ctx.shader_sprite.vao);
    assert(glGetError() == GL_NO_ERROR);

    draw_ctx.shader_sprite.attrib_camera_position = glGetUniformLocation(draw_ctx.shader_sprite.shader.program, "g_camera_position");
    draw_ctx.shader_sprite.attrib_camera_scale = glGetUniformLocation(draw_ctx.shader_sprite.shader.program, "g_camera_scale");
    draw_ctx.shader_sprite.attrib_scale = glGetUniformLocation(draw_ctx.shader_sprite.shader.program, "g_scale");
    draw_ctx.shader_sprite.attrib_use_pixel_per_units = glGetUniformLocation(draw_ctx.shader_sprite.shader.program, "g_use_pixel_per_units");

    draw_ctx.shader_sprite.attrib_quad_pos = glGetAttribLocation(draw_ctx.shader_sprite.shader.program, "v_quad_pos");
    if (draw_ctx.shader_sprite.attrib_quad_pos != -1)
    {
        glGenBuffers(1, &draw_ctx.shader_sprite.vbo_quad);
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_sprite.vbo_quad);
        glVertexAttribPointer(draw_ctx.shader_sprite.attrib_quad_pos, 2, GL_FLOAT, GL_FALSE, 0, (void*)0); // quad_pos
        glEnableVertexAttribArray(draw_ctx.shader_sprite.attrib_quad_pos);
        assert(glGetError() == GL_NO_ERROR);
    }
    draw_ctx.shader_sprite.attrib_position = glGetAttribLocation(draw_ctx.shader_sprite.shader.program, "v_position");
    if (draw_ctx.shader_sprite.attrib_position != -1)
    {
        glGenBuffers(1, &draw_ctx.shader_sprite.vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_sprite.vbo_position);
        glVertexAttribIPointer(draw_ctx.shader_sprite.attrib_position, 2, GL_INT, 0, (void*)0);
        glEnableVertexAttribArray(draw_ctx.shader_sprite.attrib_position);
        glVertexAttribDivisor(draw_ctx.shader_sprite.attrib_position, 1);
        assert(glGetError() == GL_NO_ERROR);
    }
    draw_ctx.shader_sprite.attrib_subimage = glGetAttribLocation(draw_ctx.shader_sprite.shader.program, "v_subimage");
    if (draw_ctx.shader_sprite.attrib_subimage != -1)
    {
        glGenBuffers(1, &draw_ctx.shader_sprite.vbo_subimage);
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_sprite.vbo_subimage);
        glVertexAttribIPointer(draw_ctx.shader_sprite.attrib_subimage, 1, GL_INT, 0, (void*)0);
        glEnableVertexAttribArray(draw_ctx.shader_sprite.attrib_subimage);
        glVertexAttribDivisor(draw_ctx.shader_sprite.attrib_subimage, 1);
        assert(glGetError() == GL_NO_ERROR);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    assert(glGetError() == GL_NO_ERROR);

    // setup grid shader
    draw_create_shader(&draw_ctx.shader_grid.shader, shader_vertex_grid_src, shader_fragment_grid_src);
    glGenVertexArrays(1, &draw_ctx.shader_grid.vao);
    glBindVertexArray(draw_ctx.shader_grid.vao);
    assert(glGetError() == GL_NO_ERROR);

    draw_ctx.shader_grid.attrib_camera_position = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "g_camera_position");
    draw_ctx.shader_grid.attrib_camera_scale = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "g_camera_scale");
    draw_ctx.shader_grid.attrib_scale = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "g_scale");
    draw_ctx.shader_grid.attrib_use_pixel_per_units = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "g_use_pixel_per_units");
    draw_ctx.shader_grid.attrib_position = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "g_position");
    draw_ctx.shader_grid.attrib_img_size = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "g_img_size");
    draw_ctx.shader_grid.attrib_columns = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "g_columns");
    draw_ctx.shader_grid.attrib_image_texture = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "image_texture");
    draw_ctx.shader_grid.attrib_mask_texture = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "mask_texture");
    draw_ctx.shader_grid.attrib_use_mask = glGetUniformLocation(draw_ctx.shader_grid.shader.program, "use_mask");

    draw_ctx.shader_grid.attrib_subimage = glGetAttribLocation(draw_ctx.shader_grid.shader.program, "v_subimage");
    if (draw_ctx.shader_grid.attrib_subimage != -1)
    {
        glGenBuffers(1, &draw_ctx.shader_grid.vbo_subimage);
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_grid.vbo_subimage);
        glVertexAttribIPointer(draw_ctx.shader_grid.attrib_subimage, 1, GL_INT, 0, (void*)0);
        glEnableVertexAttribArray(draw_ctx.shader_grid.attrib_subimage);
        glVertexAttribDivisor(draw_ctx.shader_grid.attrib_subimage, 1);
        assert(glGetError() == GL_NO_ERROR);
    }

    draw_ctx.shader_grid.attrib_mask_subimage = glGetAttribLocation(draw_ctx.shader_grid.shader.program, "v_mask_subimage");
    if (draw_ctx.shader_grid.attrib_mask_subimage != -1)
    {
        glGenBuffers(1, &draw_ctx.shader_grid.vbo_mask_subimage);
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_grid.vbo_mask_subimage);
        glVertexAttribIPointer(draw_ctx.shader_grid.attrib_mask_subimage, 1, GL_INT, 0, (void*)0);
        glEnableVertexAttribArray(draw_ctx.shader_grid.attrib_mask_subimage);
        glVertexAttribDivisor(draw_ctx.shader_grid.attrib_mask_subimage, 1);
        assert(glGetError() == GL_NO_ERROR);
    }
}

void draw_destroy()
{
    draw_destroy_shader(&draw_ctx.shader_primitive.shader);
    draw_destroy_shader(&draw_ctx.shader_sprite.shader);

    assert(glGetError() == GL_NO_ERROR);
}

void draw_clear(const Color& color)
{
    glClearColor(color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    assert(glGetError() == GL_NO_ERROR);
}

void draw_line(int start_x, int start_y, int stop_x, int stop_y, Color color, int scale, ScaleType scale_type)
{
    int pos_data[] = {start_x, start_y, stop_x, stop_y};

    glUseProgram(draw_ctx.shader_primitive.shader.program);
    glBindVertexArray(draw_ctx.shader_primitive.vao);

    glUniform2i(draw_ctx.shader_primitive.attrib_camera_position, Camera.x, Camera.y);
    glUniform2f(draw_ctx.shader_primitive.attrib_camera_scale, draw_ctx.width_scale, draw_ctx.height_scale);
    glUniform1i(draw_ctx.shader_primitive.attrib_scale, scale);
    glUniform1i(draw_ctx.shader_primitive.attrib_use_pixel_per_units, scale_type == ScaleType::UNITS_PER_PIXEL ? 0 : 1);
    glUniform4f(draw_ctx.shader_primitive.attrib_color, color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);

    if (draw_ctx.shader_primitive.vbo_positions != 0)
    {
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_primitive.vbo_positions);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pos_data), pos_data, GL_STREAM_DRAW);
    }

    glDrawArrays(GL_LINES, 0, 2);
    glDrawArrays(GL_POINTS, 1, 1); // Fix last pixel (diamond exit rule)

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    assert(glGetError() == GL_NO_ERROR);
}

void draw_sprite(const SpriteResource* img, int x, int y, int subimage, int scale, ScaleType scale_type)
{
    Point p{x, y};
    draw_sprites(img, &p, &subimage, 1, scale, scale_type);
}

void draw_sprites(const SpriteResource* img, const Point* coordinates, const int* subimages, int count, int scale, ScaleType scale_type)
{
    float vertices[] = {
        // clang-format off
        (float)0,          (float)0,            // top left
        (float)img->width, (float)0,            // top right
        (float)0,          (float)img->height,  // bottom left
        (float)img->width, (float)img->height,  // bottom right
        // clang-format on
    };
    glUseProgram(draw_ctx.shader_sprite.shader.program);
    glBindVertexArray(draw_ctx.shader_sprite.vao);
    assert(glGetError() == GL_NO_ERROR);

    glUniform2i(draw_ctx.shader_sprite.attrib_camera_position, Camera.x, Camera.y);
    glUniform2f(draw_ctx.shader_sprite.attrib_camera_scale, draw_ctx.width_scale, draw_ctx.height_scale);
    glUniform1i(draw_ctx.shader_sprite.attrib_scale, scale);
    glUniform1i(draw_ctx.shader_sprite.attrib_use_pixel_per_units, scale_type == ScaleType::UNITS_PER_PIXEL ? 0 : 1);

    assert(glGetError() == GL_NO_ERROR);

    if (draw_ctx.shader_sprite.attrib_quad_pos != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_sprite.vbo_quad);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STREAM_DRAW);
        assert(glGetError() == GL_NO_ERROR);
    }
    if (draw_ctx.shader_sprite.attrib_position != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_sprite.vbo_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(Point) * count, coordinates, GL_STREAM_DRAW);
        assert(glGetError() == GL_NO_ERROR);
    }
    if (draw_ctx.shader_sprite.attrib_subimage != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_sprite.vbo_subimage);
        glBufferData(GL_ARRAY_BUFFER, sizeof(int) * count, subimages, GL_STREAM_DRAW);
        assert(glGetError() == GL_NO_ERROR);
    }

    glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)img->data_gl);
    assert(glGetError() == GL_NO_ERROR);

    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, count);
    assert(glGetError() == GL_NO_ERROR);

    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
    assert(glGetError() == GL_NO_ERROR);
}

void draw_grid(const SpriteResource* img, int left, int top, const int* subimages, int columns, int rows, int scale, ScaleType scale_type)
{
    glUseProgram(draw_ctx.shader_grid.shader.program);
    glBindVertexArray(draw_ctx.shader_grid.vao);
    assert(glGetError() == GL_NO_ERROR);

    glUniform2i(draw_ctx.shader_grid.attrib_camera_position, Camera.x, Camera.y);
    glUniform2f(draw_ctx.shader_grid.attrib_camera_scale, draw_ctx.width_scale, draw_ctx.height_scale);
    glUniform1i(draw_ctx.shader_grid.attrib_scale, scale);
    glUniform1i(draw_ctx.shader_grid.attrib_use_pixel_per_units, scale_type == ScaleType::UNITS_PER_PIXEL ? 0 : 1);
    glUniform2i(draw_ctx.shader_grid.attrib_position, left, top);
    glUniform2i(draw_ctx.shader_grid.attrib_img_size, img->width, img->height);
    glUniform1i(draw_ctx.shader_grid.attrib_columns, columns);
    glUniform1i(draw_ctx.shader_grid.attrib_use_mask, 0);

    assert(glGetError() == GL_NO_ERROR);

    if (draw_ctx.shader_grid.attrib_subimage != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_grid.vbo_subimage);
        glBufferData(GL_ARRAY_BUFFER, sizeof(int) * columns * rows, subimages, GL_STREAM_DRAW);
        assert(glGetError() == GL_NO_ERROR);
    }

    glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)img->data_gl);
    assert(glGetError() == GL_NO_ERROR);

    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, columns * rows);
    assert(glGetError() == GL_NO_ERROR);

    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
    assert(glGetError() == GL_NO_ERROR);
}

void draw_grid_masked(const SpriteResource* img,
                      const SpriteResource* mask,
                      int left,
                      int top,
                      const int* subimages,
                      const int* mask_subimages,
                      int columns,
                      int rows,
                      int scale,
                      ScaleType scale_type)
{
    glUseProgram(draw_ctx.shader_grid.shader.program);
    glBindVertexArray(draw_ctx.shader_grid.vao);
    assert(glGetError() == GL_NO_ERROR);

    glUniform2i(draw_ctx.shader_grid.attrib_camera_position, Camera.x, Camera.y);
    glUniform2f(draw_ctx.shader_grid.attrib_camera_scale, draw_ctx.width_scale, draw_ctx.height_scale);
    glUniform1i(draw_ctx.shader_grid.attrib_scale, scale);
    glUniform1i(draw_ctx.shader_grid.attrib_use_pixel_per_units, scale_type == ScaleType::UNITS_PER_PIXEL ? 0 : 1);
    glUniform2i(draw_ctx.shader_grid.attrib_position, left, top);
    glUniform2i(draw_ctx.shader_grid.attrib_img_size, img->width, img->height);
    glUniform1i(draw_ctx.shader_grid.attrib_columns, columns);
    glUniform1i(draw_ctx.shader_grid.attrib_use_mask, 1);

    assert(glGetError() == GL_NO_ERROR);

    if (draw_ctx.shader_grid.attrib_subimage != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_grid.vbo_subimage);
        glBufferData(GL_ARRAY_BUFFER, sizeof(int) * columns * rows, subimages, GL_STREAM_DRAW);
        assert(glGetError() == GL_NO_ERROR);
    }
    if (draw_ctx.shader_grid.attrib_mask_subimage != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, draw_ctx.shader_grid.vbo_mask_subimage);
        glBufferData(GL_ARRAY_BUFFER, sizeof(int) * columns * rows, mask_subimages, GL_STREAM_DRAW);
        assert(glGetError() == GL_NO_ERROR);
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)img->data_gl);
    glUniform1i(draw_ctx.shader_grid.attrib_image_texture, 0);
    assert(glGetError() == GL_NO_ERROR);

    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)mask->data_gl);
    glUniform1i(draw_ctx.shader_grid.attrib_mask_texture, 1);

    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, columns * rows);
    assert(glGetError() == GL_NO_ERROR);

    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
    assert(glGetError() == GL_NO_ERROR);
}

void draw_printscreen(void* buffer, int buffer_byte_size, int* out_bytes_written)
{
    const int size_needed = Camera.width * Camera.height * 4;
    if (size_needed > buffer_byte_size)
    {
        if (out_bytes_written)
            *out_bytes_written = 0;
        return;
    }
    glFlush();
    glReadPixels(0, 0, Camera.width, Camera.height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    assert(glGetError() == GL_NO_ERROR);

    if (out_bytes_written)
    {
        *out_bytes_written = size_needed;
    }
    // Now we need to flip Y axis
    size_t ROW_SIZE = Camera.width * 4;
    uint8_t tmp[ROW_SIZE]; // TODO: make 4k buffer on bss and nestle loops
    for (int y = 0; y < Camera.height / 2; ++y)
    {
        uint8_t* top = (uint8_t*)buffer + y * ROW_SIZE;
        uint8_t* bottom = (uint8_t*)buffer + (Camera.height - y - 1) * ROW_SIZE;
        memcpy(tmp, top, ROW_SIZE);
        memcpy(top, bottom, ROW_SIZE);
        memcpy(bottom, tmp, ROW_SIZE);
    }
}

void draw_flip()
{
    nuklear_render(Camera.width, Camera.height);
    window_swap_buffers();

    int64_t delta = 0;
    if (Time.update_period_target_us > 0)
    {
        int64_t next_target = (Time.frame_no + 1) * Time.update_period_target_us;
        int64_t now;
        now = time_now();
        delta = next_target - now;
    }
#ifdef __EMSCRIPTEN__
    if (delta < 0)
        delta = 0;
    emscripten_sleep((delta + 500) / 1000);
#else
    if (delta > 0)
        std::this_thread::sleep_for(std::chrono::duration(std::chrono::microseconds(delta)));
#endif

    Time.frame_no++;
    int64_t prev_timestamp = Time.frame_timestamp_us;
    Time.frame_timestamp_us = time_now();
    Time.frame_delta_us = Time.frame_timestamp_us - prev_timestamp;

    window_update_resize();
    draw_clear();
}

void draw_resource_load(SpriteResource* sprite_resource)
{
    assert(sprite_resource != nullptr);

#ifdef __EMSCRIPTEN__
    assert(sprite_resource->ram_data == nullptr);

    std::string file = std::string("/resources/data/") + sprite_resource->name + ".bin";
    size_t filesize = sprite_resource->width * sprite_resource->height * 4 * sprite_resource->no_subimages;
    int fd = open(file.c_str(), O_RDONLY);
    assert(fd != -1);
    sprite_resource->ram_data = (uint32_t*)mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
#endif

    assert(sprite_resource->ram_data != nullptr);

    glGenTextures(1, (GLuint*)&sprite_resource->data_gl);
    glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)sprite_resource->data_gl);
    glTexImage3D(GL_TEXTURE_2D_ARRAY,
                 0,
                 GL_RGBA8,
                 sprite_resource->width,
                 sprite_resource->height,
                 sprite_resource->no_subimages,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 0);
    glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
                    0,
                    0,
                    0,
                    0,
                    sprite_resource->width,
                    sprite_resource->height,
                    sprite_resource->no_subimages,
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    sprite_resource->ram_data);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //TODO: investigate with rotation
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    assert(glGetError() == GL_NO_ERROR);

#ifdef __EMSCRIPTEN__
    munmap((void*)sprite_resource->ram_data, filesize);
    sprite_resource->ram_data = 0;
    close(fd);
#endif
}

void draw_resource_unload(SpriteResource* sprite_resource)
{
    assert(sprite_resource != nullptr);
    glDeleteTextures(1, (GLuint*)&sprite_resource->data_gl);
    sprite_resource->data_gl = 0;

    assert(glGetError() == GL_NO_ERROR);
}

void draw_set_size(int width, int height)
{
    glViewport(0, 0, width, height);
    Camera.width = width;
    Camera.height = height;
    draw_ctx.width = width;
    draw_ctx.height = height;
    draw_ctx.width_scale = 2.0f / Camera.width;
    draw_ctx.height_scale = -2.0f / Camera.height;
}

} // namespace tpx
