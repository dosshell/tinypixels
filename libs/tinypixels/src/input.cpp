#include "input_internal.h"

#include "gui_internal.h"
#include "clock_internal.h"
#include "window_internal.h"

#include <GLFW/glfw3.h>

namespace tpx
{
InputType Input;

void scroll_callback(GLFWwindow*, double, double yoffset)
{
    Input.scroll += (int)yoffset;
}

void input_init()
{
    GLFWwindow* glfw_window = (GLFWwindow*)window_get_glfw_window();
#ifndef __EMSCRIPTEN__
    glfwSetInputMode(glfw_window, GLFW_STICKY_KEYS, GLFW_TRUE);
    glfwSetInputMode(glfw_window, GLFW_STICKY_MOUSE_BUTTONS, GLFW_TRUE);
#endif
    glfwSetScrollCallback(glfw_window, scroll_callback);
}

void input_update()
{
    Input.scroll = 0;
    glfwPollEvents();
    Input.timestamp = tpx::time_now();
    GLFWwindow* glfw_window = (GLFWwindow*)window_get_glfw_window();

    std::array<bool, Button::COUNT> new_state = {};

    for (int i = 0; i < GLFW_KEY_LAST; ++i)
    {
        new_state[i] = glfwGetKey(glfw_window, i) == GLFW_PRESS;
    }

    new_state[Button::MOUSE_1] = glfwGetMouseButton(glfw_window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;
    new_state[Button::MOUSE_2] = glfwGetMouseButton(glfw_window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS;
    new_state[Button::MOUSE_3] = glfwGetMouseButton(glfw_window, GLFW_MOUSE_BUTTON_3) == GLFW_PRESS;

    for (int i = 0; i < Button::COUNT; ++i)
    {
        Input.is_released[i] = Input.is_down[i] && !new_state[i];
        Input.is_pressed[i] = !Input.is_down[i] && new_state[i];
        Input.is_down[i] = new_state[i];
    }

    Input.should_close = glfwWindowShouldClose(glfw_window);

    double x;
    double y;
    glfwGetCursorPos(glfw_window, &x, &y);
    Input.mouse_x = (int)x;
    Input.mouse_y = (int)y;

    nuklear_update_input();
}

} // namespace tpx
