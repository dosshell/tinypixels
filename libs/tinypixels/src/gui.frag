#version 300 es

precision mediump float;

uniform mediump sampler2DArray Texture;
in vec2 Frag_UV;
in vec4 Frag_Color;
out vec4 Out_Color;

void main(){
    Out_Color = Frag_Color * texture(Texture, vec3(Frag_UV.st, 0));
}
