#include "sound.h"

#include "miniaudio.h"

#include <cstring> // memcpy
#include <cstdio>  // snprintf
#include <mutex>   // std::mutex
#include <stdint.h>
#include <cassert>
#include <array>
#include <atomic>
#include <algorithm> // std::min

namespace
{
struct VfsFile
{
    tpx::SoundResource* resource;
    int64_t cursor;
    std::mutex mut;

    static ma_result open(ma_vfs*, const char* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
    {
        if (openMode != MA_OPEN_MODE_READ)
            return MA_ACCESS_DENIED;

        auto file = new VfsFile{};
        auto resource_ptr = (tpx::SoundResource*)strtoull(pFilePath, NULL, 16);
        file->resource = resource_ptr;
        *pFile = (ma_vfs_file)file;

        return MA_SUCCESS;
    }

    static ma_result openw(ma_vfs* pVFS, const wchar_t* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
    {
        return open(pVFS, (const char*)pFilePath, openMode, pFile);
    }

    static ma_result close(ma_vfs*, ma_vfs_file file)
    {
        delete (VfsFile*)file;
        return MA_SUCCESS;
    }

    static ma_result read(ma_vfs*, ma_vfs_file file, void* pDst, size_t sizeInBytes, size_t* pBytesRead)
    {
        auto vfs_file = (VfsFile*)file;
        std::scoped_lock<std::mutex> m{vfs_file->mut};

        int64_t bytes_left = vfs_file->resource->size - vfs_file->cursor;
        int64_t bytes_to_read = std::min(bytes_left, (int64_t)sizeInBytes);
        assert(bytes_to_read >= 0);
        memcpy(pDst, reinterpret_cast<const uint8_t* const>(vfs_file->resource->data) + vfs_file->cursor, bytes_to_read);
        vfs_file->cursor += bytes_to_read;

        if (pBytesRead != nullptr)
            *pBytesRead = bytes_to_read;

        return bytes_to_read > 0 ? MA_SUCCESS : MA_AT_END;
    }

    static ma_result write(ma_vfs*, ma_vfs_file, const void*, size_t, size_t* pBytesWritten)
    {
        if (pBytesWritten != nullptr)
            *pBytesWritten = 0;
        return MA_ACCESS_DENIED;
    }

    static ma_result seek(ma_vfs*, ma_vfs_file file, ma_int64 offset, ma_seek_origin origin)
    {
        VfsFile* vfs_file = reinterpret_cast<VfsFile*>(file);
        std::scoped_lock lock{vfs_file->mut};

        switch (origin)
        {
            case ma_seek_origin_start:
                vfs_file->cursor = offset;
                break;
            case ma_seek_origin_current:
                vfs_file->cursor += offset;
                break;
            case ma_seek_origin_end:
                vfs_file->cursor = vfs_file->resource->size + offset;
                break;
        }
        return MA_SUCCESS;
    }

    static ma_result tell(ma_vfs*, ma_vfs_file file, ma_int64* pCursor)
    {
        VfsFile* vfs_file = reinterpret_cast<VfsFile*>(file);
        if (pCursor != nullptr)
        {
            std::scoped_lock lock{vfs_file->mut};
            *pCursor = vfs_file->cursor;
        }
        return MA_SUCCESS;
    }

    static ma_result info(ma_vfs*, ma_vfs_file file, ma_file_info* pInfo)
    {
        if (pInfo != nullptr)
            pInfo->sizeInBytes = reinterpret_cast<VfsFile*>(file)->resource->size;
        return MA_SUCCESS;
    }
};

ma_engine s_engine{};
ma_default_vfs s_vfs{};

class SoundChannelPool
{
public:
    SoundChannelPool(const size_t sound_limit) : SOUND_LIMIT(sound_limit)
    {
        for (size_t i = 0; i < CACHE_SIZE; ++i)
        {
            m_callback_data[i].index = i;
            m_callback_data[i].pool = this;
        }
    }

    bool pull_sound(const tpx::SoundResource* sound_resource, ma_sound** out_sound)
    {
        if (out_sound == nullptr)
            return false;

        if (++m_sounds_in_use_count > SOUND_LIMIT)
        {
            m_sounds_in_use_count--;
            return false;
        }

        for (size_t i = 0; i < CACHE_SIZE; ++i)
        {
            if (m_resources[i] == sound_resource)
            {
                bool expected = false;
                if (m_in_use[i].compare_exchange_strong(expected, true))
                {
                    *out_sound = &m_sounds[i];
                    return true;
                }
            }
        }
        for (size_t i = 0; i < CACHE_SIZE; ++i)
        {
            bool expected = false;
            if (m_in_use[i].compare_exchange_strong(expected, true))
            {
                if (m_resources[i] != nullptr)
                    ma_sound_uninit(&m_sounds[i]);
                m_resources[i] = sound_resource;
                char buffer[2 + 16 + 1] = {};                                 // 0x + hex + \0 (platform dependent format)
                snprintf(buffer, sizeof buffer, "%p", (void*)sound_resource); // TODO: Move to resource manager or use data sources
                ma_sound_init_from_file(&s_engine, buffer, MA_SOUND_FLAG_STREAM, NULL, NULL, &m_sounds[i]);
                m_sounds[i].pEndCallbackUserData = reinterpret_cast<void*>(&m_callback_data[i]);
                m_sounds[i].endCallback = [](void* user_data, ma_sound*)
                {
                    auto callback_data = reinterpret_cast<CallbackData*>(user_data);
                    SoundChannelPool* pool = callback_data->pool;
                    size_t index = callback_data->index;

                    // Order will change behaviour of aquire
                    pool->m_in_use[index] = false;
                    pool->m_sounds_in_use_count--;
                };
                *out_sound = &m_sounds[i];
                return true;
            }
        }
        return false;
    }

    void clear()
    {
        for (size_t i = 0; i < CACHE_SIZE; ++i)
        {
            if (m_resources[i] != nullptr)
                ma_sound_uninit(&m_sounds[i]);
            m_resources[i] = nullptr;
            m_in_use[i] = false;
        }
        m_sounds_in_use_count = 0;
    }

    static constexpr size_t CACHE_SIZE = 64;
    const int64_t SOUND_LIMIT;
    std::atomic_int64_t m_sounds_in_use_count = 0;
    ma_sound m_sounds[CACHE_SIZE]{};
    std::atomic_bool m_in_use[CACHE_SIZE]{};
    const tpx::SoundResource* m_resources[CACHE_SIZE]{};
    std::atomic<float> volume{1.0f};
    struct CallbackData
    {
        size_t index;
        SoundChannelPool* pool;
    } m_callback_data[CACHE_SIZE]{};
};

// Same order as `enum class SoundChannel`
std::array<class SoundChannelPool, 3> s_sound_channel_pools = {SoundChannelPool(24), SoundChannelPool(6), SoundChannelPool(2)};

} // namespace
namespace tpx
{
bool sound_init()
{
    ma_default_vfs_init(&s_vfs, NULL);
    s_vfs.cb.onOpen = VfsFile::open;
    s_vfs.cb.onOpenW = VfsFile::openw;
    s_vfs.cb.onClose = VfsFile::close;
    s_vfs.cb.onRead = VfsFile::read;
    s_vfs.cb.onWrite = VfsFile::write;
    s_vfs.cb.onSeek = VfsFile::seek;
    s_vfs.cb.onTell = VfsFile::tell;
    s_vfs.cb.onInfo = VfsFile::info;

    ma_engine_config engine_config = ma_engine_config_init();
    engine_config.pResourceManagerVFS = &s_vfs;
    ma_result result = ma_engine_init(&engine_config, &s_engine);

    if (result != MA_SUCCESS)
    {
        return false;
    }

    return true;
}

void sound_destroy()
{
    for (auto& pool : s_sound_channel_pools)
        pool.clear();

    ma_engine_uninit(&s_engine);
}

bool sound_play(const SoundResource* sound_resource, SoundChannel sound_channel)
{
    if (sound_resource == nullptr)
        return false;

    SoundChannelPool* pool = &s_sound_channel_pools[(int)sound_channel];
    ma_sound* sound;
    if (!pool->pull_sound(sound_resource, &sound))
        return false;
    assert(sound != nullptr);

    ma_sound_set_volume(sound, pool->volume);
    if (ma_sound_start(sound) != MA_SUCCESS)
        return false;
    return true;
}

bool sound_playing_count(SoundChannel sound_channel, int64_t* out_number_of_sounds)
{
    if (out_number_of_sounds == nullptr)
        return false;

    // Counter can be higher during trying to pull a sound
    int64_t max_counter = (int64_t)s_sound_channel_pools[(size_t)sound_channel].SOUND_LIMIT;
    int64_t counter = s_sound_channel_pools[(int)sound_channel].m_sounds_in_use_count;
    if (counter > max_counter)
        counter = max_counter;

    *out_number_of_sounds = counter;
    return true;
}

bool sound_stop(SoundChannel sound_channel)
{
    for (size_t i = 0; i < SoundChannelPool::CACHE_SIZE; ++i)
    {
        if (s_sound_channel_pools[(int)sound_channel].m_resources[i] != nullptr)
        {
            ma_sound* sound = &s_sound_channel_pools[(int)sound_channel].m_sounds[i];
            bool was_stopped_ok = ma_sound_stop(sound);
            bool was_rewinded_ok = ma_sound_seek_to_pcm_frame(sound, 0);
            assert(was_stopped_ok);
            assert(was_rewinded_ok);
        }
    }

    return true;
}

bool sound_pause(SoundChannel sound_channel)
{
    for (size_t i = 0; i < SoundChannelPool::CACHE_SIZE; ++i)
    {
        if (s_sound_channel_pools[(int)sound_channel].m_resources[i] != nullptr)
        {
            ma_sound* sound = &s_sound_channel_pools[(int)sound_channel].m_sounds[i];
            bool was_paused_ok = ma_sound_stop(sound);
            assert(was_paused_ok);
        }
    }
    return true;
}

bool sound_resume(SoundChannel sound_channel)
{
    for (size_t i = 0; i < SoundChannelPool::CACHE_SIZE; ++i)
    {
        if (s_sound_channel_pools[(int)sound_channel].m_resources[i] != nullptr)
        {
            ma_sound* sound = &s_sound_channel_pools[(int)sound_channel].m_sounds[i];
            if (!ma_sound_at_end(sound))
            {
                bool was_resumed_ok = ma_sound_start(sound);
                assert(was_resumed_ok);
            }
        }
    }
    return true;
}

bool sound_set_volume(SoundChannel sound_channel, float volume)
{
    s_sound_channel_pools[(int)sound_channel].volume = volume;
    for (size_t i = 0; i < SoundChannelPool::CACHE_SIZE; ++i)
    {
        if (s_sound_channel_pools[(int)sound_channel].m_resources[i] != nullptr)
        {
            ma_sound* sound = &s_sound_channel_pools[(int)sound_channel].m_sounds[i];
            ma_sound_set_volume(sound, volume);
        }
    }
    return true;
}

} // namespace tpx
