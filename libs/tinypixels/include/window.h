#pragma once

namespace tpx
{
constexpr int NO_MAX_FPS = 0;

bool window_open(const char* title,
                 int width,
                 int height,
                 bool full_screen = false,
                 bool resizable = false,
                 bool vsync = true,
                 int max_fps = NO_MAX_FPS);
bool window_close();
void window_flip(); // Draw and input and everything else
} // namespace tpx
