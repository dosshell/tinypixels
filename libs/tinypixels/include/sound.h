#pragma once

#include <stdint.h>

namespace tpx
{

struct SoundResource
{
    const void* const data; // value of data gets corrupted
    const int64_t size;
};

enum class SoundChannel
{
    Sfx,
    Misc,
    Music,
    All, // do not use for play
};

bool sound_play(const SoundResource* sound_resource, SoundChannel sound_channel);
// bool sound_play_at(const SoundResource* sound_resource, SoundChannel sound_channel, int64_t ms_offset);
// bool audio_play2d(const SoundResource* sound_resource, SoundChannel sound_channel, int x, int y);
// bool audio_play2d_(const SoundResource* sound_resource, SoundChannel sound_channel, int x, int y, int64_t ms_offset);

bool sound_playing_count(SoundChannel sound_channel, int64_t* out_number_of_sounds);
bool sound_stop(SoundChannel sound_channel);
bool sound_pause(SoundChannel sound_channel);
bool sound_resume(SoundChannel sound_channel);
bool sound_set_volume(SoundChannel sound_channel, float volume);

} // namespace tpx
