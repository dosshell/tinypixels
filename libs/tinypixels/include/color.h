#pragma once

#include <stdint.h>
#include <array>

namespace tpx
{
class Color
{
public:
    Color() : rgba{0, 0, 0, 0xff}
    {
    }
    Color(const Color& c) = default;

    Color(int r, int g, int b, int a = 0xFF) : rgba{uint8_t(r), uint8_t(g), uint8_t(b), uint8_t(a)}
    {
    }

    Color(uint32_t hex)
        : rgba{uint8_t((hex & 0xff000000) >> 24), uint8_t((hex & 0x00ff0000) >> 16), uint8_t((hex & 0x0000ff00) >> 8), uint8_t(hex & 0x000000ff)}
    {
    }

    Color(uint8_t* bytes) : rgba{uint8_t(bytes[0]), uint8_t(bytes[1]), uint8_t(bytes[2]), uint8_t(bytes[3])}
    {
    }

    Color& operator=(const Color& c) = default;

    Color& operator=(uint32_t hex) noexcept
    {
        *this = Color(hex);
        return *this;
    }

    bool operator==(const Color& other) const
    {
        return other.r == r && other.g == g && other.b == b && other.a == a;
    }

    uint8_t operator[](int n) const
    {
        return rgba[n];
    }

    uint8_t& operator[](int n)
    {
        return rgba[n];
    }

    union
    {
        std::array<uint8_t, 4> rgba;
        struct
        {
            uint8_t r;
            uint8_t g;
            uint8_t b;
            uint8_t a;
        };
    };
};
} // namespace tpx