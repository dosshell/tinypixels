#pragma once

#include "color.h"

#include <stdint.h>

namespace tpx
{
struct CameraType
{
    int x;      // moves camera directly for future draw calls
    int y;      // moves camera directly for future draw calls
    int width;  // updates after flip if changed
    int height; // updates after flip if changed
};

struct SpriteResource
{
    const char* name;
    int width;
    int height;
    int pivot_x;
    int pivot_y;
    int no_subimages;
    const uint32_t* ram_data;
    uint32_t data_gl;
};

// Todo: should not need explicit pack here
#pragma pack(push, 1)
struct Point
{
    int32_t x;
    int32_t y;
};
#pragma pack(pop)

enum class ScaleType
{
    UNITS_PER_PIXEL,
    PIXELS_PER_UNIT,
};

void draw_clear(const Color& color = 0x000000FF);
void draw_line(int start_x, int start_y, int stop_x, int stop_y, Color color, int scale = 1, ScaleType scale_type = ScaleType::UNITS_PER_PIXEL);
void draw_sprite(const SpriteResource* img, int x, int y, int subimage = 0, int scale = 1, ScaleType scale_type = ScaleType::UNITS_PER_PIXEL);
void draw_sprites(const SpriteResource* img,
                  const Point* coordinates,
                  const int* subimages,
                  int count,
                  int scale = 1,
                  ScaleType scale_type = ScaleType::UNITS_PER_PIXEL);
void draw_grid(const SpriteResource* img,
               int left,
               int top,
               const int* subimages,
               int columns,
               int rows,
               int scale = 1,
               ScaleType scale_type = ScaleType::UNITS_PER_PIXEL);
void draw_grid_masked(const SpriteResource* img,
                      const SpriteResource* mask,
                      int left,
                      int top,
                      const int* subimages,
                      const int* mask_subimages,
                      int columns,
                      int rows,
                      int scale = 1,
                      ScaleType scale_type = ScaleType::UNITS_PER_PIXEL);

void draw_printscreen(void* buffer, int buffer_byte_size, int* out_byte_written);

void draw_flip();

void draw_resource_load(SpriteResource* sprite_resource);
void draw_resource_unload(SpriteResource* sprite_resource);

extern CameraType Camera;
} // namespace tpx
