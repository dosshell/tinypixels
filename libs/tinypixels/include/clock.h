#pragma once

#include <stdint.h>

namespace tpx
{
struct TimeType
{
    int64_t start_timestamp_us;
    int64_t frame_timestamp_us;
    int64_t frame_delta_us;
    int64_t frame_no;
    int64_t update_period_target_us; // in us
};

// Value in us
int64_t time_now();

inline TimeType Time{};
} // namespace tpx