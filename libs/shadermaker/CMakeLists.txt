if (EMSCRIPTEN)
    add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/libs/shadermaker/shadermaker
        COMMAND g++ ${CMAKE_CURRENT_SOURCE_DIR}/src/shadermaker.cpp -o ${CMAKE_BINARY_DIR}/libs/shadermaker/shadermaker
        )
    add_custom_target(shadermaker_custom_target SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/shadermaker.cpp DEPENDS ${CMAKE_BINARY_DIR}/libs/shadermaker/shadermaker)
    add_executable(shadermaker alias shadermaker_custom_target)
else ()
    add_executable(shadermaker src/shadermaker.cpp)
endif ()
