#include "stb_image.h"

#include <exception>
#include <ios>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <memory>
#include <string>
#include <set>
#include <vector>
#include <map>
#include <regex>
#include <stdint.h>
#include <cstring>

namespace fs = std::filesystem;

struct SpriteMakerData
{
    std::string path;
    int width;
    int height;
    int stride;
    std::vector<uint32_t*> subimages;
};

struct SoundMakerData
{
    std::string name;
    std::string path;
};

void file_to_hex(fs::path input_path)
{
    std::string var_name = input_path.stem().string();
    std::ifstream input{input_path, std::ios::binary | std::ios::ate};
    size_t filesize = input.tellg();
    input.seekg(0);

    std::ofstream output{input_path.replace_extension(".hex")};

    output << "int64_t incbin_" << var_name << "_size = " << filesize << ";" << std::endl;
    output << std::hex;

    output << "const unsigned char incbin_" << var_name << "_data[] = {\n";

    auto b = std::make_unique<unsigned char[]>(filesize);
    input.read((char*)(b.get()), filesize);
    for (int i = 0; i < filesize; ++i)
    {
        output << "0x" << std::hex << (int)b[i] << ",";
    }

    output << "};\n";
}

// WARNING: This is a pice of crap. Do not look.

int main(const int argc, const char* argv[])
{
    if (argc != 3)
    {
        std::cout << "argument error" << std::endl;
        return -1;
    }
    const char* input_dir = argv[1];
    std::string output_dir = argv[2];
    std::string data_dir = output_dir + "/data/";

    try
    {
        std::filesystem::create_directories(data_dir);
    }
    catch (const std::exception& e)
    {
        std::cout << "Failed to create directory: " << data_dir << "\nError message: " << e.what() << std::endl;
        exit(-1);
    }

    std::map<std::string, SpriteMakerData, std::less<>> sprites;
    std::map<std::string, SoundMakerData, std::less<>> sounds;
    std::regex reg_strip("^(.*)_(\\d+)$");
    std::regex reg_map("^(.*)_(\\d+)x(\\d+)(_autotile)?$");
    std::regex reg_autotile(".*_autotile$");

    // Read images
    std::cout << "reading from: " << input_dir << std::endl;
    // read in alphabetic order
    std::set<fs::path> sorted_by_name;
    for (auto& entry : fs::directory_iterator(input_dir))
        if (entry.is_regular_file())
            sorted_by_name.insert(entry.path());

    for (const fs::path& file_path : sorted_by_name)
    {
        if (file_path.extension().compare(".png") == 0)
        {
            std::ifstream t(file_path);
            int width, height, nrChannels;
            stbi_uc* stb = stbi_load(file_path.string().c_str(), &width, &height, &nrChannels, 0);
            uint32_t* data = reinterpret_cast<uint32_t*>(stb);

            if (nrChannels == 4)
            {
                // Identify format
                std::string name = file_path.stem().string();
                bool is_strip = std::regex_match(name, reg_strip);
                bool is_map = std::regex_match(name, reg_map);
                bool is_autotile = std::regex_match(name, reg_autotile);
                bool is_single = !is_strip && !is_map;

                if (is_autotile)
                {
                    if (!is_map)
                    {
                        std::cout << "warning: needs to be a map!" << std::endl;
                    }
                    std::smatch sm;
                    if (!std::regex_search(name, sm, reg_map))
                    {
                        std::cout << "warning: fatal error" << std::endl;
                        continue;
                    }
                    SpriteMakerData smd;
                    std::string key_name = sm[1];
                    smd.path = data_dir + key_name + ".bin";
                    int columns = std::stoi(sm[2]);
                    int rows = std::stoi(sm[3]);
                    smd.width = width / columns;
                    smd.height = height / rows;
                    smd.stride = width;
                    uint32_t* data_ptr[257] = {nullptr};
                    data_ptr[0] = data;
                    bool is_ground[columns][rows];
                    memset(is_ground, 0, columns * rows * sizeof(bool));

                    for (int y = 0; y < rows; ++y)
                    {
                        for (int x = 0; x < columns; ++x)
                        {
                            size_t offset = x * smd.width + smd.width / 2 + y * width * smd.height + (width * smd.height / 2);
                            is_ground[x][y] = (*(data + offset) & 0xFF000000) > 0;
                        }
                    }

                    for (int y = 1; y < rows - 1; ++y)
                    {
                        for (int x = 1; x < columns - 1; ++x)
                        {
                            if (is_ground[x][y])
                            {
                                int b1 = is_ground[x - 1][y - 1];
                                int b2 = is_ground[x + 0][y - 1];
                                int b3 = is_ground[x + 1][y - 1];
                                int b4 = is_ground[x - 1][y + 0];
                                int b5 = is_ground[x + 1][y + 0];
                                int b6 = is_ground[x - 1][y + 1];
                                int b7 = is_ground[x + 0][y + 1];
                                int b8 = is_ground[x + 1][y + 1];

                                int n = 1 + (b1 | b2 << 1 | b3 << 2 | b4 << 3 | b5 << 4 | b6 << 5 | b7 << 6 | b8 << 7);
                                if (data_ptr[n] == nullptr)
                                {
                                    data_ptr[n] = data + y * width * smd.height + x * smd.width;
                                }
                            }
                        }
                    }

                    for (int c = 0; c < 257; ++c)
                    {
                        smd.subimages.push_back(data_ptr[c] ? data_ptr[c] : data_ptr[256]);
                    }
                    sprites.try_emplace(key_name, smd);
                }
                else if (is_single)
                {
                    SpriteMakerData smd;
                    smd.path = data_dir + name + ".bin";
                    smd.height = height;
                    smd.width = width;
                    smd.stride = width;

                    smd.subimages.push_back(data);
                    sprites.try_emplace(name, smd);
                }
                else if (is_strip)
                {
                    std::string key_name = name.substr(0, name.rfind("_"));
                    if (sprites.count(key_name) == 0)
                    {
                        SpriteMakerData smd;
                        smd.path = data_dir + key_name + ".bin";
                        smd.width = width;
                        smd.height = height;
                        smd.stride = width;
                        smd.subimages.push_back(data);
                        sprites.try_emplace(key_name, smd);
                    }
                    else
                    {
                        sprites[key_name].subimages.push_back(data);
                    }
                }
                else if (is_map)
                {
                    std::smatch sm;
                    if (!std::regex_search(name, sm, reg_map))
                        continue;

                    SpriteMakerData smd;
                    std::string key_name = sm[1];
                    smd.path = data_dir + key_name + ".bin";
                    int columns = std::stoi(sm[2]);
                    int rows = std::stoi(sm[3]);
                    smd.width = width / columns;
                    smd.height = height / rows;
                    smd.stride = width;
                    for (int r = 0; r < rows; ++r)
                    {
                        for (int c = 0; c < columns; ++c)
                        {
                            smd.subimages.push_back(data + c * smd.width + r * smd.stride * smd.height);
                        }
                    }
                    sprites.try_emplace(key_name, smd);
                }
            }
            else
            {
                std::cout << "warning: " << file_path << " does not contain 4 channels" << std::endl;
            }
        }
        if (file_path.extension().compare(".mp3") == 0 || file_path.extension().compare(".ogg") == 0)
        {
            std::string name = file_path.stem().string();
            std::string data_path = data_dir + name + ".bin";
            {
                std::ifstream t(file_path);

                SoundMakerData smd;
                smd.name = name;
                smd.path = file_path.string();
                sounds.try_emplace(name, smd);
                try
                {
                    const auto copy_options = fs::copy_options::overwrite_existing;
                    fs::copy_file(file_path, data_path, copy_options);
                }
                catch (const std::exception& e)
                {
                    std::cout << "Failed to copy: " << data_path << "\nError message: " << e.what() << std::endl;
                    exit(-1);
                }
            }
            file_to_hex(data_path);
        }
    }
    // Log detected resources
    for (auto const& [name, smd] : sprites)
    {
        std::cout << "Detected resource [sprite]: " << name << std::endl;
    }
    for (auto const& [name, smd] : sounds)
    {
        std::cout << "Detected resource [sound]: " << name << std::endl;
    }

    // Write bin files if needed
    for (auto const& [name, smd] : sprites)
    {
        std::string bin_file = data_dir + name + ".bin";
        {
            std::cout << "Writing: " << bin_file << std::endl;
            std::ofstream f(bin_file, std::ios::binary | std::ios::out | std::ios::trunc);
            if (!f)
            {
                perror("Could not open file for writing");
                continue;
            }
            for (int subindex = 0; subindex < smd.subimages.size(); ++subindex)
            {
                for (int r = 0; r < smd.height; ++r)
                {
                    const char* p = reinterpret_cast<const char*>(smd.subimages[subindex]);
                    f.write(p + r * smd.stride * 4, smd.width * 4);
                }
            }
        }
        file_to_hex(bin_file);
    }

    // Write header file
    {
        std::ofstream out_header(output_dir + "/resources.h");
        out_header << ""
                      "// This file is generated by resouremaker"
                      "\n"
                      "#pragma once\n"
                      "\n"
                      "#include <tinypixels.h>\n"
                      "\n";
        out_header << "namespace resources\n{\n";

        for (auto const& [name, smd] : sprites)
        {
            out_header << "extern tpx::SpriteResource sprite_" << name << ";\n";
        }
        for (auto const& [name, smd] : sounds)
        {
            out_header << "extern tpx::SoundResource sound_" << name << ";\n";
        }
        out_header << "\n";
        out_header << "void load_all();\n";
        out_header << "void unload_all();\n";
        out_header << "} // namespace resources\n";
    }

    // Write cpp file
    {
        // Header
        std::ofstream out_cpp(output_dir + "/resources.cpp");
        out_cpp << ""
                   "// This file is generated by resouremaker"
                   "#include \"resources.h\"\n"
                   "\n"
                   "#include <tinypixels.h>\n"
                   "\n"
                   "#ifndef __EMSCRIPTEN__\n"
                   "#define INCBIN_STYLE INCBIN_STYLE_SNAKE\n"
                   "#define INCBIN_PREFIX incbin_\n"
                   "#include <incbin.h>\n"
                   "#else\n"
                   "#define INCBIN(X, Y) const unsigned char * incbin_##X##_data = nullptr;\n"
                   "#endif\n"
                   "\n"
                   "#include <cstdint>"
                   "\n"
                   "\n";
        out_cpp << "namespace resources\n{\n";

        out_cpp << "#ifdef _WIN32\n";
        // include hex
        for (auto const& [name, smd] : sprites)
        {
            out_cpp << "#include \"data/" << name << ".hex\"\n";
        }
        for (auto const& [name, smd] : sounds)
        {
            out_cpp << "#include \"data/" << name << ".hex\"\n";
        }
        // INCBIN
        out_cpp << "#else\n";
        for (auto const& [name, smd] : sprites)
        {
            out_cpp << "INCBIN(" << name << ", \"" << smd.path << "\");\n";
        }
        for (auto const& [name, smd] : sounds)
        {
            out_cpp << "INCBIN(" << smd.name << ", \"" << smd.path << "\");\n";
        }
        out_cpp << "#endif\n";

        out_cpp << "\n";

        // Sprite resources
        for (auto const& [name, smd] : sprites)
        {
            out_cpp << "tpx::SpriteResource sprite_" << name << "{"
                    << "\"" << name << "\""
                    << ", " << smd.width << ", " << smd.height << ", 0, 0, " << smd.subimages.size() << ", (const uint32_t*)incbin_" << name
                    << "_data};\n";
        }

        for (auto const& [name, smd] : sounds)
        {
            out_cpp << "tpx::SoundResource sound_" << name << "{(void*)incbin_" << smd.name << "_data, incbin_" << smd.name << "_size};\n";
        }
        out_cpp << "\n";

        out_cpp << "void load_all()\n{\n";
        for (const auto& [name, seconds] : sprites)
        {
            out_cpp << "    tpx::draw_resource_load(&sprite_" << name << ");\n";
        }
        out_cpp << "}\n";

        out_cpp << "void unload_all()\n{\n";
        for (const auto& [name, seconds] : sprites)
        {
            out_cpp << "    tpx::draw_resource_unload(&sprite_" << name << ");\n";
        }
        out_cpp << "}\n";
        out_cpp << "} // namespace resources\n";
    }
    std::cout << output_dir << std::endl;
    return 0;
}
