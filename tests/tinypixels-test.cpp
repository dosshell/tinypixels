#include <tinypixels.h>

#include <resources.h>

#include <doctest.h>
#include <thread>
#include <chrono>

#if _MSC_VER
#pragma warning(disable : 6319)
#endif

using namespace resources;

constexpr int WIDTH = 800;
constexpr int HEIGHT = 600;
constexpr int PIXELS = WIDTH * HEIGHT;
constexpr int PIXEL_BYTES = PIXELS * 4;

namespace tpx
{

TEST_CASE("Colors")
{
    // Given
    REQUIRE(sizeof(Color) == 4);
    const Color white{0xFFFFFFFF};
    const Color black{0, 0, 0};
    Color copy_ctr_white = white;
    Color copy{};
    auto func = [](Color a) -> Color { return a; };

    // When
    copy = black;

    // Then
    CHECK(func(0xFF0000FF) == Color(255, 0, 0));
    CHECK(copy == black);
    CHECK(white != black);
    CHECK(copy_ctr_white == white);
}

TEST_CASE("init game")
{
    // Given
    bool vsync = true;
    bool fullscreen = false;
    int width = 800;
    int height = 600;

    // When
    bool start_code = window_open("TinyPixels Test", width, height, fullscreen, vsync, NO_MAX_FPS);

    // Then
    CHECK(start_code);

    // Cleanup
    REQUIRE(window_close());
}

TEST_CASE("game double open and close")
{
    // Given

    // When
    CHECK(window_open("TinyPixels Test", 800, 600));
    CHECK(window_close());
    CHECK(window_open("TinyPixels Test", 800, 600));
    CHECK(window_close());

    // Then
}

TEST_CASE("tear up")
{
    REQUIRE(window_open("TinyPixels Test", 800, 600));
}

TEST_CASE("draw clear")
{
    // Given
    Color c = 0x12345678;
    int bytes = 0;
    uint8_t pixels[PIXEL_BYTES] = {};

    // When
    draw_clear(c);
    draw_printscreen(pixels, PIXEL_BYTES, &bytes);
    draw_flip();

    // Then
    CHECK(bytes == PIXEL_BYTES);
    CHECK(c == &pixels[0]);
}

TEST_CASE("draw flip")
{
    // Given

    // When
    draw_clear(0xFFFFFFFF);
    draw_flip();

    // Then
    uint8_t pixels[PIXEL_BYTES] = {};
    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    CHECK(pixels[0] != 0xFF);
}

TEST_CASE("draw line")
{
    // Given
    const int units_per_pixel = 2;

    draw_clear();

    // When
    input_update();
    Camera.x = -2'000;
    Camera.y = 2'000;
    draw_line((Camera.x + 1) * units_per_pixel + 1,
              (Camera.y + 1) * units_per_pixel + 1,
              (Camera.x + 3) * units_per_pixel + 1,
              (Camera.y + 3) * units_per_pixel + 1,
              0xEDCBA9FF,
              units_per_pixel);

    // Then
    Color pixels[HEIGHT][WIDTH] = {};

    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    draw_flip();

    CHECK(pixels[0][0] == 0x000000FF);
    CHECK(pixels[1][1] == 0xEDCBA9FF);
    CHECK(pixels[2][2] == 0xEDCBA9FF);
    CHECK(pixels[3][3] == 0xEDCBA9FF);
    CHECK(pixels[4][4] == 0x000000FF);
}

TEST_CASE("sprite resource load and unload")
{
    // Given
    CHECK(sprite_test_single.data_gl == 0);

    // When
    draw_resource_load(&sprite_test_single);
    int gl_data = sprite_test_single.data_gl;
    draw_resource_unload(&sprite_test_single);

    // Then
    CHECK(gl_data != 0);
    CHECK(sprite_test_single.data_gl == 0);
}

TEST_CASE("draw single sprite")
{
    // Given
    const int units_per_pixel = 2;
    draw_resource_load(&sprite_test_single);
    draw_clear();
    Camera.x = 32;
    Camera.y = -32;
    int left_draw = 128;
    int top_draw = 96;
    int screen_left = left_draw / units_per_pixel - Camera.x;
    int screen_top = top_draw / units_per_pixel - Camera.y;
    int screen_bottom = screen_top + sprite_test_single.height - 1;
    int screen_right = screen_left + sprite_test_single.width - 1;

    // When
    input_update();
    draw_sprite(&sprite_test_single, left_draw, top_draw, 0, units_per_pixel);

    // Then
    Color pixels[HEIGHT][WIDTH] = {};

    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    draw_flip();

    CHECK(pixels[screen_top][screen_left] == 0xFF0000FF);
    CHECK(pixels[screen_bottom][screen_left] == 0x0000FFFF);
    CHECK(pixels[screen_top][screen_right] == 0x00FF00FF);
    CHECK(pixels[screen_bottom][screen_right] == 0x000000FF);

    draw_resource_unload(&sprite_test_single);
}

TEST_CASE("draw multi sprite")
{
    // Given
    draw_resource_load(&sprite_test_multi);
    draw_clear();
    Camera.x = 0;
    Camera.y = 0;

    // When
    input_update();
    Point points[3] = {{0, 0}, {300, 0}, {0, 300}};
    int subimages[3] = {0, 1, 2};
    draw_sprites(&sprite_test_multi, points, subimages, 3);

    // Then
    Color pixels[HEIGHT][WIDTH] = {};

    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    draw_flip();

    CHECK(pixels[0][0] == 0xEC1C24FF);
    CHECK(pixels[0][300] == 0x3F48CCFF);
    CHECK(pixels[300][0] == 0x0ED145FF);

    draw_resource_unload(&sprite_test_multi);
}

TEST_CASE("draw grid")
{
    // Given
    draw_resource_load(&sprite_test_multi);
    draw_clear();
    Camera.x = 0;
    Camera.y = 0;

    // When
    input_update();
    int subimages[] = {0, 1, 2, 1, 2, 0};
    draw_grid(&sprite_test_multi, 32, 16, subimages, 3, 2);

    // Then
    Color pixels[HEIGHT][WIDTH] = {};

    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    draw_flip();

    CHECK(pixels[0][0] == 0x000000FF);
    CHECK(pixels[16][32] == 0xEC1C24FF);
    CHECK(pixels[16][332] == 0x3F48CCFF);
    CHECK(pixels[316][332] == 0x0ED145FF);

    draw_resource_unload(&sprite_test_multi);
}

TEST_CASE("text")
{
    // Given
    draw_resource_load(&sprite_font_roboto_mono);
    Camera.x = 0;
    Camera.y = 0;

    constexpr char32_t msg[] = U"Hello World!";
    constexpr size_t LENGTH = std::char_traits<char32_t>::length(msg);

    // When
    draw_grid(&sprite_font_roboto_mono, 64, 64, (int*)msg, LENGTH, 1);

    // Then
    Color pixels[HEIGHT][WIDTH] = {};
    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    draw_flip();

    CHECK(pixels[64][64] == 0x000000FF);
    CHECK(pixels[72][66] == 0xFEFEFEFE);

    // Cleanup
    draw_resource_unload(&sprite_font_roboto_mono);
}

TEST_CASE("Draw sprite grid masked")
{
    // Given
    draw_resource_load(&sprite_rainbow);
    draw_resource_load(&sprite_mask);
    Camera.x = 0;
    Camera.y = 0;
    int subimages[6] = {0, 0, 0, 0, 0, 0};
    int mask_subimages[6] = {0, 0, 0, 0, 0, 0};

    // When
    draw_grid_masked(&sprite_rainbow, &sprite_mask, 32, 32, subimages, mask_subimages, 3, 2);

    // Then
    Color pixels[HEIGHT][WIDTH] = {};
    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    draw_flip();

    CHECK(pixels[32][32] == 0xFF0000FF);
    CHECK(pixels[64][64] == 0x000000FF);

    // Cleanup
    draw_resource_unload(&sprite_rainbow);
    draw_resource_unload(&sprite_mask);
}

TEST_CASE("Pixels per unit")
{
    // Given
    draw_resource_load(&sprite_rainbow);
    draw_resource_load(&sprite_mask);
    Camera.x = 0;
    Camera.y = 0;
    int subimages[6] = {0, 0, 0, 0, 0, 0};
    int mask_subimages[6] = {0, 0, 0, 0, 0, 0};

    // When
    draw_grid_masked(&sprite_rainbow, &sprite_mask, 1, 1, subimages, mask_subimages, 3, 2, 32, ScaleType::PIXELS_PER_UNIT);
    draw_line(5, 3, 6, 4, 0xEDCBA9FF, 32, ScaleType::PIXELS_PER_UNIT);

    // Then
    Color pixels[HEIGHT][WIDTH] = {};
    draw_printscreen(pixels, PIXEL_BYTES, nullptr);
    draw_flip();

    CHECK(pixels[32][32] == 0xFF0000FF);
    CHECK(pixels[64][64] == 0x000000FF);
    CHECK(pixels[3 * 32][5 * 32] == 0xEDCBA9FF);
    CHECK(pixels[4 * 32][6 * 32] == 0xEDCBA9FF);

    // Cleanup
    draw_resource_unload(&sprite_rainbow);
    draw_resource_unload(&sprite_mask);
}

TEST_CASE("Nuklear")
{
    struct nk_colorf bg = {};

    for (int i = 0; i < 1; ++i)
    {
        input_update();

        if (nk_begin(&Nuklear,
                     "Demo",
                     nk_rect(50, 50, 230, 250),
                     NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE | NK_WINDOW_CLOSABLE | NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE))
        {
            enum
            {
                EASY,
                HARD
            };
            static int op = EASY;
            static int property = 20;
            nk_layout_row_static(&Nuklear, 30, 80, 1);
            if (nk_button_label(&Nuklear, "button"))
                fprintf(stdout, "button pressed\n");

            nk_layout_row_dynamic(&Nuklear, 30, 2);
            if (nk_option_label(&Nuklear, "easy", op == EASY))
                op = EASY;
            if (nk_option_label(&Nuklear, "hard", op == HARD))
                op = HARD;

            nk_layout_row_dynamic(&Nuklear, 25, 1);
            nk_property_int(&Nuklear, "Compression:", 0, &property, 100, 10, 1);

            nk_layout_row_dynamic(&Nuklear, 20, 1);
            nk_label(&Nuklear, "background:", NK_TEXT_LEFT);
            nk_layout_row_dynamic(&Nuklear, 25, 1);
            if (nk_combo_begin_color(&Nuklear, nk_rgb_cf(bg), nk_vec2(nk_widget_width(&Nuklear), 400)))
            {
                nk_layout_row_dynamic(&Nuklear, 120, 1);
                bg = nk_color_picker(&Nuklear, bg, NK_RGBA);
                nk_layout_row_dynamic(&Nuklear, 25, 1);
                bg.r = nk_propertyf(&Nuklear, "#R:", 0, bg.r, 1.0f, 0.01f, 0.005f);
                bg.g = nk_propertyf(&Nuklear, "#G:", 0, bg.g, 1.0f, 0.01f, 0.005f);
                bg.b = nk_propertyf(&Nuklear, "#B:", 0, bg.b, 1.0f, 0.01f, 0.005f);
                bg.a = nk_propertyf(&Nuklear, "#A:", 0, bg.a, 1.0f, 0.01f, 0.005f);
                nk_combo_end(&Nuklear);
            }
        }
        nk_end(&Nuklear);
        draw_flip();
    }
}

TEST_CASE("miniaudio")
{
    // Given
    Camera.x = 0;
    Camera.y = 0;
    draw_flip();

    CHECK(sound_play(&resources::sound_car_horn_vorbis, SoundChannel::Sfx));
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // When
    CHECK(sound_play(&resources::sound_car_horn_mp3, SoundChannel::Sfx));
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // Then
}

TEST_CASE("miniaudio spam")
{
    // Given
    for (int i = 0; i < 24; ++i)
    {
        CHECK(sound_play(&resources::sound_car_horn_vorbis, SoundChannel::Sfx));
    }
    int64_t sounds_playing = 0;
    CHECK(sound_playing_count(SoundChannel::Sfx, &sounds_playing));
    CHECK(sounds_playing == 24);

    // When
    for (int i = 0; i < 8; ++i)
    {
        CHECK(sound_play(&resources::sound_car_horn_vorbis, SoundChannel::Sfx) == false);
    }

    // Then
    CHECK(sound_playing_count(SoundChannel::Sfx, &sounds_playing));
    CHECK(sounds_playing == 24);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
}

TEST_CASE("tear down")
{
    REQUIRE(window_close());
}

} // namespace tpx
