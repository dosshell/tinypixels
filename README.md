# Tinypixels

Data oriented 2d render library.

## Emscripten

Activate emsdk
```
./emsdk activate latest
source ./emsdk_env.sh
emcc -v  # verify paths are ok
```

Build tinypixels
```
mkdir embuild
cd embuild
emcmake cmake -DTPX_ENABLE_TESTS=True ..
emmake make -j 8
python3 -m http.server -b 127.0.0.1 -d tests/
```
