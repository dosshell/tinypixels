

#include <stdio.h>
#include <stdlib.h>

char* dtoa(char *s, double n)
{
    snprintf(s, 64, "%f", n);
    return s;
}

// Wrapper for lost of const warning
double strtod_const(const char *s, const char **endptr)
{
    return strtod(s, (char**)endptr);
}

#define NK_IMPLEMENTATION

#define NK_MEMSET memset
#define NK_MEMCPY memcpy
#define NK_SIN sinf
#define NK_COS cosf
#define NK_STRTOD strtod_const
#define NK_DTOA dtoa
#define NK_VSNPRINTF vsnprintf

#include "nuklear.h"
